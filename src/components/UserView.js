import { useState, useEffect } from 'react';
// import productData from './../data/products'
import ProductCard from './ProductCard'

export default function UserView({productsProp}) {

	const [productsArr, setProductsArr] = useState([])
	

	useEffect(() => {
		const products = productsProp.map(product => {
			// console.log(product)
			if(product.isActive){
				return <ProductCard key={product._id} productProp={product} />
			}else{
				return null
			}
		})

		setProductsArr(products)

	}, [productsProp])

	return(
		<>
			{productsArr}
		</>
	)
}