import { useState, useEffect, useContext } from 'react';
// import productData from './../data/products'
import UserView from  "../components/UserView"
import AdminView from  "../components/AdminView"
import UserContext from "../UserContext"

export default function Products() {

	const { user } = useContext(UserContext);

	const [productsData, setProductsData] = useState([])


	const fetchData = () => {
		fetch(`${ process.env.REACT_APP_API_URL }/products`)
		.then(res => res.json())
		.then(data => {

			setProductsData(data)
		})
	}


	useEffect(() => {
		fetchData()
	}, [])

	return(user.isAdmin ?
		<AdminView productsProp={productsData} fetchData={fetchData}/>
		:
		<UserView productsProp={productsData}/>)
}