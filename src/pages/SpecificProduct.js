
import { useState, useEffect, useContext } from 'react';
import { Link, useParams, useHistory } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function SpecificProduct(){

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")

	const { user } = useContext(UserContext);

	const { productId } = useParams();

	const history = useHistory();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])

	const enroll = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data){
				Swal.fire({
					title: "Added to Cart",
					icon: "success",
					text: `You have added ${name} to cart`
				})

				history.push("/products")
			}else{
				Swal.fire({
					title: "Failed Cart Add",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	return(
		<Card className="mt-3">
			<Card.Header className="bg-dark text-white text-center pb-0">
				<h4>{name}</h4>
			</Card.Header>
			<Card.Body>
				<Card.Text>{description}</Card.Text>
				<h6>Price: {price}</h6>
			</Card.Body>
			<Card.Footer className="d-grid gap-2">
				{user.id !== null ?
					<Button variant="primary" block="true" onClick={enroll}>Add to Cart</Button>
					:
					<Link className="btn btn-danger btn-block" to="/login">Log In to Enroll</Link>
				}
			</Card.Footer>
		</Card>
	)
}